package vehicle;
import java.util.Calendar;
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.IOException;

public class Bicycle extends Vehicle {
	private int gears;
	private Calendar productionDate;
	public Bicycle() {}
	public Bicycle(String name, String colour, int price, int model, String serialNumber, int direction, int gears) {
		super(name, colour, price, model, serialNumber, direction);
		this.gears = gears;
		productionDate = new java.util.GregorianCalendar();
	}
	
	@Override
	public void turnLeft(int degrees) {
		System.out.print("Bicycle turned " + degrees + "degrees left");
	}
	@Override
	public void turnRight(int degrees) {
		System.out.print("Bicycle turned " + degrees + "degrees right");
	}
	public int getGears() {
		return gears;
	}
	public void setGears(int gears) {
		this.gears = gears;
	}
	public Calendar getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(Calendar date) {
		productionDate = date;
	}
	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Gears: ");
		gears = input.nextInt();
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public String toString() {
		return super.toString() + String.format(" Gears: %d Production date: %d-%d-%d", gears, productionDate.get(Calendar.YEAR), 
		    	   																			   productionDate.get(Calendar.MONTH), 
		    	   																			   productionDate.get(Calendar.DAY_OF_MONTH));
	}
	@Override
	public Object clone() {
		Bicycle bicycle = (Bicycle)super.clone();
		bicycle.setGears(gears);
		bicycle.setProductionDate((Calendar)productionDate.clone());
		return bicycle;
	}
	@Override
	public void accelerate(double factor) {
		double newSpeed;
		if (getSpeed() == 0) {
			newSpeed = 0.3 * factor;
		}
		else {
			newSpeed = getSpeed() * 0.5 * factor;
		}
		if (newSpeed > MAX_SPEED_BIKE) {
			newSpeed = MAX_SPEED_BIKE;
		}
		setSpeed(newSpeed);
		System.out.println("Vehicle accelerated to: " + getSpeed() + " km/h");
	}
	@Override
	public void breaks(double factor) {
		setSpeed(getSpeed() / (factor * 0.5));
		System.out.println("Vehicle slowed down to: " + getSpeed() + " km/h");
	}
	@Override
	public void writeData(PrintWriter w) throws IOException {
		super.writeData(w);
		w.printf(",%d,%s", getGears(), Common.formatCalendar(getProductionDate()));
		if(w.checkError())
			throw new IOException();
	}
	@Override
	public void readData(Scanner in) throws IOException {
		super.readData(in);
		try {
			setGears(in.nextInt());
			setProductionDate(Common.parseCalendar(in.next()));
		} catch(Exception e) {
			throw new IOException("Exception occurred on parsing CSV values", e);
		}
	}
}
