package vehicle;

import java.util.Calendar;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;

public class Car extends Vehicle {
	private int power;
	private Calendar productionDate;
	public Car() {}
	public Car(String name, String colour, int price, int model, String serialNumber, int direction, int power) {
		super(name, colour, price, model, serialNumber, direction);
		this.power = power;
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Power: ");
		power = input.nextInt();
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public void turnLeft(int degrees) {
		if (degrees >= 0 && degrees <= 360) {
			super.setDirection((super.getDirection() - degrees + 360) % 360);
		}
	}
	@Override
	public void turnRight(int degrees) {
		if (degrees >= 0 && degrees <= 360) {
			super.setDirection((super.getDirection() + degrees) % 360);
		}
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public Calendar getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(Calendar date) {
		productionDate = date;
	}
	@Override
	public String toString() {
		return super.toString() + String.format(" Power: %d Production date: %d-%d-%d", power, productionDate.get(Calendar.YEAR), 
																					    	   productionDate.get(Calendar.MONTH), 
																					    	   productionDate.get(Calendar.DAY_OF_MONTH));
	}
	@Override
	public Object clone() {
		Car car = (Car)super.clone();
		car.setPower(power);
		car.setProductionDate((Calendar)productionDate.clone());
		return car;
	}
	@Override
	public void accelerate(double factor) {
		double newSpeed;
		if (getSpeed() == 0) {
			newSpeed = 0.5 * factor;
		}
		else {
			newSpeed = getSpeed() * factor;
		}
		if (newSpeed > MAX_SPEED_CAR) {
			newSpeed = MAX_SPEED_CAR;
		}
		setSpeed(newSpeed);
		System.out.println("Vehicle accelerated to: " + getSpeed() + " km/h");
	}
	@Override
	public void breaks(double factor) {
		setSpeed(getSpeed() / factor);
		System.out.println("Vehicle slowed down to: " + getSpeed() + " km/h");
	}
	@Override
	public void writeData(PrintWriter w) throws IOException {
		super.writeData(w);
		w.printf(",%d,%s", getPower(), Common.formatCalendar(getProductionDate()));
		if(w.checkError())
			throw new IOException();
	}
	@Override
	public void readData(Scanner in) throws IOException {
		super.readData(in);
		try {
			setPower(in.nextInt());
			setProductionDate(Common.parseCalendar(in.next()));
		} catch(Exception e) {
			throw new IOException("Exception occurred on parsing CSV values", e);
		}
	}
}
