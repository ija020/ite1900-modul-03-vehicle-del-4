package vehicle;

import java.util.Calendar;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Scanner;
import java.text.ParseException;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class Vehicle implements Comparable<Vehicle>, Cloneable, Driveable, Fileable {
	private String colour, name, serialNumber;
	private int model, price, direction;
	private double speed;
	protected java.util.Scanner input = new java.util.Scanner(System.in);
	private Calendar buyingDate;
	public Vehicle() {}
	public Vehicle(String name, String colour, int price, int model, String serialNumber, int direction) {
		this.name = name;
		this.colour = colour;
		this.price = price;
		this.model = model;
		this.serialNumber = serialNumber;
		this.direction = direction;
		speed = 0;
		buyingDate = new java.util.GregorianCalendar();
	}
	public void setAllFields() {
		System.out.println("Input car data:");
		System.out.print("Name: ");
		name = input.next();
		System.out.print("Colour: ");
		colour = input.next();
		System.out.print("Price: ");
		price = input.nextInt();
		System.out.print("Model: ");
		model = input.nextInt();
		System.out.print("Serial #: ");
		serialNumber = input.next();
		direction = 0;
		speed = 0;
		buyingDate = new java.util.GregorianCalendar();
	}
	public abstract void turnLeft(int degrees);
	public abstract void turnRight(int degrees);
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public Calendar getBuyingDate() {
		return buyingDate;
	}
	public void setBuyingDate(Calendar date) {
		buyingDate = date;
	}
	@Override
	public String toString() {
		return String.format("Name: %s Colour: %s Price: %d Model: %d Serial#: %s Direction: %d Speed: %f", name, colour, price, model, serialNumber, direction, speed);
	}
	@Override
	public int compareTo(Vehicle vehicle) {
		if (this.getPrice() - vehicle.getPrice() > 0 && this.getPrice() - vehicle.getPrice() < 1) {
			return 1;
		}
		else if (this.getPrice() - vehicle.getPrice() < 0 && this.getPrice() - vehicle.getPrice() > -1){
			return -1;
		}
		return this.getPrice() - vehicle.getPrice();
	}
	@Override
	public Object clone() {
		Vehicle vehicle = null;
		try {
			vehicle = (Vehicle)super.clone();
			vehicle.setName(name);
			vehicle.setColour(colour);
			vehicle.setPrice(price);
			vehicle.setModel(model);
			vehicle.setSerialNumber(serialNumber);
			vehicle.setDirection(direction);
			vehicle.setSpeed(speed);
			vehicle.setBuyingDate((Calendar)buyingDate.clone());
		} catch (CloneNotSupportedException e) {}
		return vehicle;
	}
	@Override
	public void stop() {
		speed = 0;
		System.out.println("Vehicle stops");
	}
	private static enum Field {
		NAME, COLOUR, PRICE, MODEL, SERIAL_NUMBER, DIRECTION, SPEED, BUYING_DATE
	}
	public void writeData(PrintWriter out) throws IOException {
		for(int i = 0; i < Field.values().length; i++) {
			out.print(getField(Field.values()[i]));
			if(i + 1 != Field.values().length)
				out.print(",");
		}
		if(out.checkError())
			throw new IOException();
	}
	private String getField(Field field) {
		switch(field) {
		case NAME:
			return getName();
		case COLOUR:
			return getColour();
		case PRICE:
			return Integer.valueOf(getPrice()).toString();
		case MODEL:
			return Integer.valueOf(getModel()).toString();
		case SERIAL_NUMBER:
			return getSerialNumber();
		case DIRECTION:
			return Integer.valueOf(getDirection()).toString();
		case SPEED:
			return Double.valueOf(getSpeed()).toString();
		case BUYING_DATE:
			return Common.formatCalendar(getBuyingDate());
		default:
			throw new AssertionError("Unrecognized field " + field.toString());
		}
	}
	private void setField(Field field, String value) throws NumberFormatException, ParseException {
		switch(field) {
		case NAME:
			setName(value);
			break;
		case COLOUR:
			setColour(value);
			break;
		case PRICE:
			setPrice(Integer.parseInt(value));
			break;
		case MODEL:
			setModel(Integer.parseInt(value));
			break;
		case SERIAL_NUMBER:
			setSerialNumber(value);
			break;
		case DIRECTION:
			setDirection(Integer.parseInt(value));
			break;
		case SPEED:
			setSpeed(Double.parseDouble(value));
			break;
		case BUYING_DATE:
			setBuyingDate(Common.parseCalendar(value));
			break;
		default:
			throw new AssertionError("Unrecognized field " + field.toString());
		}
	}
	public void readData(Scanner in) throws IOException {
		in.useDelimiter(",");
		String[] csvValues = new String[Field.values().length];
		for(int i = 0; i < Field.values().length; i++)
			csvValues[i] = in.next();
		try {
			for(int i = 0; i < Field.values().length; i++) {
				setField(Field.values()[i], csvValues[i]);
			}
		} catch(Exception e) {
			throw new IOException("Exception occurred on parsing CSV values", e);
		}
	}
}
