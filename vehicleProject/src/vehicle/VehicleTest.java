package vehicle;
/**
*  TestVehicles oppretter Bicycle og Car objekter, legger disse i et ArrayList
*  Lar bruker manipulere data i arrayet p� forskjellige m�ter
*/
import java.util.*;
import java.io.*;

public class VehicleTest {
	private static final String DATA_FILENAME = "vehicles.csv";
	public static void main(String[] args) {
		VehicleTest vtest = new VehicleTest();
		try {
			vtest.menuLoop();
		}
		catch(IOException e) {
			System.out.println("IO Exception!");
			System.exit(1);
		}
		catch(CloneNotSupportedException e) {
			System.out.println("CloneNotSupportedException");
			System.exit(1);
			}
		}
	private static List<Vehicle> loadVehicles() throws IOException {
		List<Vehicle> list = new ArrayList<>();
		Scanner file = new Scanner(new File(DATA_FILENAME));
		while(file.hasNextLine()) {
			Scanner line = new Scanner(file.nextLine());
			line.useDelimiter(",");
			String className = line.next();
			try {
				Class<?> klass = Class.forName(className);
				Vehicle vehicle = (Vehicle)klass.getDeclaredConstructor().newInstance();
				vehicle.readData(line);
				System.out.printf("Vehicle read from file: %s.\n", vehicle.toString());
				list.add(vehicle);
			} catch(ReflectiveOperationException e) {
				throw new IOException("Failed to instantiate class", e);
			}
		}
		return list;
	}
	private static void saveVehicles(List<Vehicle> list) throws IOException {
		PrintWriter file = new PrintWriter(DATA_FILENAME);
		for(Vehicle v : list) {
			file.printf("%s,", v.getClass().getName());
			v.writeData(file);
			file.println();
			System.out.printf("Vehicle written from file: %s.\n", v.toString());
		}
	}
	private void menuLoop() throws IOException, CloneNotSupportedException {
		Scanner scan = new Scanner(System.in);
		List<Vehicle> arr = loadVehicles();
		Vehicle vehicle;
		while(true) {
			System.out.println("1...................................New car");
			System.out.println("2...............................New bicycle");
			System.out.println("3......................Find vehicle by name");
			System.out.println("4..............Show data about all vehicles");
			System.out.println("5.......Change direction of a given vehicle");
			System.out.println("6.........................Test clone method");
			System.out.println("7..................Test driveable interface");
			System.out.println("8..............................Exit program");
			System.out.println(".............................Your choice?");
			int choice = scan.nextInt();
			String name;
			switch (choice) {
			case 1:
				vehicle = new Car();
				vehicle.setAllFields();
				arr.add(vehicle);
				break;
			case 2:
				Bicycle bicycle = new Bicycle();
				bicycle.setAllFields();
				arr.add(bicycle);
				break;
			case 3:
				System.out.println("Name of vehicle: ");
				name = scan.next();
				for (int i = 0; i < arr.size(); i++) {
					if (arr.get(i).getName().equals(name)) {
						System.out.println(arr.get(i).toString());
						break;
					}
					else if (i == arr.size() - 1) {
						System.out.println("Vehicle not found");
					}
				}
				break;
			case 4:
				for (int i = 0; i < arr.size(); i++) {
					System.out.println(arr.get(i).toString());
				}
				break;
			case 5:
				System.out.println("Name of vehicle: ");
				name = scan.next();
				for (int i = 0; i < arr.size(); i++) {
					if (arr.get(i).getName().equals(name)) {
						System.out.println("Direction [R/L]: ");
						String direction = scan.next();
						System.out.println("Degrees [0-360]: ");
						int degrees = scan.nextInt();
						if (direction.equals("R")) {
							arr.get(i).turnRight(degrees);
						}
						else if (direction.equals("L")) {
							arr.get(i).turnLeft(degrees);
						}
						else {
							System.out.println("Illegal direction");
						}
						break;
					}
					else if (i == arr.size() - 1) {
						System.out.println("Vehicle not found");
					}
				}
				break;
			case 6:
				Car car = new Car("CloneTest","Green",77777,2007,"B777-77",150,0);
				Car carClone = (Car)car.clone();
				carClone.setProductionDate(new java.util.GregorianCalendar(2005, 1, 1));
				if (car.getProductionDate() != carClone.getProductionDate()) {
					System.out.println("Date objects are separate, deep copy.");
				}
				System.out.println(String.format("%d-%d-%d", car.getProductionDate().get(Calendar.YEAR), 
														     car.getProductionDate().get(Calendar.MONTH), 
														     car.getProductionDate().get(Calendar.DAY_OF_MONTH)));
				
				System.out.println(String.format("%d-%d-%d", carClone.getProductionDate().get(Calendar.YEAR), 
														     carClone.getProductionDate().get(Calendar.MONTH), 
														     carClone.getProductionDate().get(Calendar.DAY_OF_MONTH)));
				break;
			case 7:
				Car testCar = new Car();
				Bicycle testBicyle = new Bicycle();
				System.out.println("Car:");
				testCar.accelerate(10);
				testCar.accelerate(1000);
				testCar.breaks(30);
				testCar.stop();
				System.out.println("Bicycle:");
				testBicyle.accelerate(10);
				testBicyle.accelerate(1000);
				testBicyle.breaks(30);
				testBicyle.stop();
				break;
			case 8:
				scan.close();
				saveVehicles(arr);
				System.exit(0);
			default:
				System.out.println("Wrong input!");
			}
			System.out.println("");
		}
	}
}
