package vehicle;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Locale;

class Common {
	public static Calendar parseCalendar(String str) throws ParseException {
		Date date = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ROOT).parse(str);
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return calendar;
	}
	public static String formatCalendar(Calendar calendar) {
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ROOT);
		Date date = calendar.getTime();
		return df.format(date);
	}
}
