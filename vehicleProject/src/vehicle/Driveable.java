package vehicle;

public interface Driveable {
	public static final double MAX_SPEED_CAR = 250;
	public static final double MAX_SPEED_BIKE = 100;
	public void accelerate(double factor);
	public void breaks(double factor);
	public void stop();
}
